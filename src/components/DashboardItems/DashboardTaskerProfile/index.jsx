import React from 'react';
import {
  Wrapper,
  LineWrapper,
  Label,
  CategoryLabel,
  Category,
  Certificate,
  TextArea,
} from './DashboardTaskerProfile.style';

const TaskerDashboardProfile = () => (
  <Wrapper>
    <LineWrapper>
      <Label>Job Title</Label>
    </LineWrapper>
    <LineWrapper>
      <Label>Introduction</Label>
    </LineWrapper>
    <TextArea>
      styled-components utilises tagged template literals to style your components. It removes the mapping between
      components and styles. This means that when youre defining your styles, youre actually creating a normal React
      component, that has your styles attached to it. This example creates two simple components, a wrapper and a title,
      with some styles attached to it:
    </TextArea>
    <LineWrapper>
      <Label>Skill Description</Label>
    </LineWrapper>
    <TextArea>
      styled-components utilises tagged template literals to style your components. It removes the mapping between
      components and styles. This means that when youre defining your styles, youre actually creating a normal React
      component, that has your styles attached to it. This example creates two simple components, a wrapper and a title,
      with some styles attached to it:
    </TextArea>
    <LineWrapper>
      <CategoryLabel>Categories</CategoryLabel>
    </LineWrapper>
    <LineWrapper>
      <Category></Category>
      <Category></Category>
    </LineWrapper>
    <LineWrapper>
      <CategoryLabel>Certificate</CategoryLabel>
    </LineWrapper>
    <LineWrapper>
      <Certificate></Certificate>
    </LineWrapper>
  </Wrapper>
);

// Above use static data now, will change to props.job_title, then, will de-comment these codes.

// TaskerDashboardProfile.propTypes = {
//   children: PropTypes.string.isRequired,
//   alertDot: PropTypes.bool,
// };

export default TaskerDashboardProfile;
