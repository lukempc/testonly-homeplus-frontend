import React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

const PaymentInput = () => {
  const boxStyle2 = { display: 'flex', flexDirection: 'column', marginTop: '10px' };

  return (
    <Box sx={{ display: 'flex', flexDirection: 'row', margin: '30px' }}>
      <Box sx={{ ...boxStyle2, marginRight: '20px', width: '150px' }}>
        <Typography>BSB</Typography>
        <TextField id="outlined-basic" size="small" sx={{ width: '100px', marginTop: '5px' }} />
      </Box>
      <Box sx={{ ...boxStyle2 }}>
        <Typography>Account Number</Typography>
        <TextField id="outlined-basic" size="small" sx={{ width: '450px', marginTop: '5px' }} />
      </Box>
    </Box>
  );
};

export default PaymentInput;
