import styled from 'styled-components';

export const ListItemText = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  span {
    font-size: 1.5rem;
    font-family: Roboto;
    font-weight: 300;
  }
`;
