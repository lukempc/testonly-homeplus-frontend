import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Badge from '@mui/material/Badge';
import LogoImg from '../../assets/logo.PNG';
import DefaultImage from '../../assets/defaultUser.PNG';
import CustomButton from '../CustomButton';
import LoginModal from '../Login';
import RegisterModal from '../Register';
import { Nav, Logo, MenuLinkAvatar, Menu, Hamburger, Container, ButtonsSet } from './Navbar.style.jsx';

const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [userID, setUserID] = useState('');
  const [avatar, setAvatar] = useState('');
  const [notifications, setNotifications] = useState(0);
  const [showLoginModal, setShowLoginModal] = useState(false);
  const [showRegisterModal, setShowRegisterModal] = useState(false);

  const openLoginModal = () => {
    setShowLoginModal(!showLoginModal);
    setShowRegisterModal(false);
  };

  const openRegisterModal = () => {
    setShowRegisterModal(!showRegisterModal);
    setShowLoginModal(false);
  };

  useEffect(() => {
    setInterval(() => {
      if (localStorage.getItem('userID') != null) {
        setUserID(JSON.parse(localStorage.getItem('userID')));
        //TODO:get the image from the backend if not use default
        setAvatar(DefaultImage);
        //number of notification
        setNotifications(JSON.parse(localStorage.getItem('notifications')));
        setShowLoginModal(false);
        setShowRegisterModal(false);
      }
    }, 500);
  }, []);

  return (
    <>
      <Nav>
        <div>
          <div>
            <Link to="/home">
              <Logo src={LogoImg}></Logo>
            </Link>
          </div>
          <Hamburger onClick={() => setIsOpen(!isOpen)}>
            <span />
            <span />
            <span />
          </Hamburger>
          <Menu isOpen={isOpen}>
            <div>
              <Link to="/post-task">
                <CustomButton color="primary" variant="contained">
                  Post a task
                </CustomButton>
              </Link>
              <Link to="/browse">
                <CustomButton color="black" variant="text">
                  Browse Tasks
                </CustomButton>
              </Link>
            </div>
            <div>
              {userID && notifications ? (
                <Badge badgeContent={notifications} color="error">
                  <MenuLinkAvatar src={avatar} href="/" />
                </Badge>
              ) : userID ? (
                <MenuLinkAvatar src={avatar} href="/" />
              ) : (
                <ButtonsSet>
                  <CustomButton color="black" onClick={openLoginModal}>
                    Login
                  </CustomButton>
                  <CustomButton color="black" onClick={openRegisterModal}>
                    Register
                  </CustomButton>
                </ButtonsSet>
              )}
            </div>
          </Menu>
        </div>
      </Nav>
      <Container>
        <LoginModal showModal={showLoginModal} setShowModal={setShowLoginModal} openRegisterModal={openRegisterModal} />
        <RegisterModal
          showModal={showRegisterModal}
          setShowModal={setShowRegisterModal}
          openLoginModal={openLoginModal}
        />
      </Container>
    </>
  );
};

export default Navbar;
