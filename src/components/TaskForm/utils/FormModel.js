export const postTaskFormModel = {
  formId: 'postTaskForm',
  formFields: {
    category: {
      name: 'category',
      label: '',
      type: 'string',
    },
    title: {
      name: 'title',
      label: 'Task title',
      type: 'string',
      requiredErrorMsg: 'Task title is required',
      minErrorMsg: 'Must be at least 10 characters',
      maxErrorMsg: 'Must be 20 characters or less',
    },
    date: {
      name: 'date',
      label: 'Task date',
      type: 'string',
    },
    certainTimeCheck: {
      name: 'certain_time_check',
      label: 'I need a certain time of day',
      type: 'boolean',
    },
    certainTime: {
      name: 'certain_time',
      label: '',
      type: 'string',
    },
    budget: {
      name: 'budget',
      label: 'Budget',
      type: 'number',
      requiredErrorMsg: 'Budget is required',
      minErrorMsg: 'Min value 1',
    },
    street: {
      name: 'street',
      label: 'Street',
      type: 'string',
      minErrorMsg: 'Must be at least 5 characters',
    },
    state: {
      name: 'state',
      label: 'State',
      type: 'string',
      minErrorMsg: 'Must be at least 3 characters',
    },
    postcode: {
      name: 'postcode',
      label: 'Postcode',
      type: 'number',
      formatErrorMsg: 'Must be correct format',
    },
    houseType: {
      name: 'house_type',
      label: 'House Type',
      type: 'string',
      chooseErrorMsg: 'Must choose one',
    },
    numOfRooms: {
      name: 'num_of_rooms',
      label: 'Number of Rooms',
      type: 'number',
    },
    numOfBath: {
      name: 'num_of_bathrooms',
      label: 'Number of Bathroom',
      type: 'number',
    },
    levels: {
      name: 'levels',
      label: 'levels',
      type: 'number',
    },
    lift: {
      name: 'lift',
      label: 'Lift avaliable',
      type: 'boolean',
    },
    inPerson: {
      name: 'in_person',
      label: 'In-person support',
      type: 'boolean',
    },
    itemName: {
      name: 'item_name',
      label: 'Item name',
      type: 'string',
    },
    itemValue: {
      name: 'item_value',
      label: 'Item value',
      type: 'number',
    },
    description: {
      name: 'description',
      label: 'More Description',
      type: 'string',
    },
  },
};
