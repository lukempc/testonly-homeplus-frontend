import { React, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import StepContent from '@mui/material/StepContent';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

import { backToInit } from '../../store/reducers/form/form.actions';
import { CustomForm } from './styles/PostForm.style';

import PostForm1 from './components/TitileTimeForm';
import PostForm2 from './components/DetailsForm';
import PostForm3 from './components/OthersForm';

const PostForm = ({ values, formBackToInit, handleSubmit }) => {
  const [activeStep, setActiveStep] = useState(0);

  const [isTitleError, setIsTitleError] = useState(false);
  const [isBudgetError, setIsBudgetError] = useState(false);
  const [first2CateErrors, setFirst2CateErrors] = useState(false);
  const [handyErrors, setHandyErrors] = useState(false);

  const validationCheck = async (index) => {
    switch (index) {
      case 0:
        if (values.title.length < 10) {
          setIsTitleError(true);
          break;
        } else {
          setIsTitleError(false);
          handleNext();
          break;
        }
      case 1:
        if (values.budget <= 0) {
          setIsBudgetError(true);
          break;
        } else {
          setIsBudgetError(false);
        }
        if (values.category === 'handy') {
          if (values.item_name === '' || !values.item_value) {
            setHandyErrors(true);
            break;
          } else {
            setHandyErrors(false);
            handleNext();
            break;
          }
        } else {
          if (values.street === '' || values.state === '' || values.postcode.length !== 4 || values.houseType === '') {
            setFirst2CateErrors(true);
            break;
          } else {
            setFirst2CateErrors(false);
            handleNext();
            break;
          }
        }

      default:
        handleNext();
        break;
    }
  };

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    formBackToInit();
    setActiveStep(0);
  };

  const steps = [
    {
      label: 'Title & Date',
      form: <PostForm1 isTitleError={isTitleError} />,
    },
    {
      label: 'Details',
      form: <PostForm2 isBudgetError={isBudgetError} handyErrors={handyErrors} first2CateErrors={first2CateErrors} />,
    },
    {
      label: 'Other information',
      form: <PostForm3 />,
    },
  ];

  return (
    <CustomForm onSubmit={handleSubmit}>
      <Box
        sx={{
          minWidth: 380,
          maxWidth: 800,
        }}
      >
        <Stepper activeStep={activeStep} orientation="vertical" sx={{ mt: 5 }}>
          {steps.map((step, index) => (
            <Step key={step.label}>
              <StepLabel optional={index === 2 ? <Typography variant="caption">Last step</Typography> : null}>
                {step.label}
              </StepLabel>
              <StepContent>
                {step.form}
                <Box sx={{ mb: 2, mt: 3 }}>
                  <div>
                    {index === steps.length - 1 ? (
                      <Button variant="contained" type="submit" onClick={handleNext} sx={{ mt: 1, mr: 1 }}>
                        Submit
                      </Button>
                    ) : (
                      <Button variant="contained" onClick={() => validationCheck(index)} sx={{ mt: 1, mr: 1 }}>
                        Continue
                      </Button>
                    )}

                    <Button disabled={index === 0} onClick={handleBack} sx={{ mt: 1, mr: 1 }}>
                      Back
                    </Button>
                  </div>
                </Box>
              </StepContent>
            </Step>
          ))}
        </Stepper>
        {activeStep === steps.length && (
          <Paper square elevation={0} sx={{ p: 3 }}>
            <Typography>All steps completed - you&apos;re finished</Typography>
            <Button onClick={handleReset} sx={{ mt: 1, mr: 1 }}>
              Reset
            </Button>
          </Paper>
        )}
      </Box>
    </CustomForm>
  );
};

PostForm.propTypes = {
  values: PropTypes.object.isRequired,
  formBackToInit: PropTypes.func,
  handleSubmit: PropTypes.func,
};

const mapDispatchToProps = (dispatch) => ({
  formBackToInit: () => dispatch(backToInit()),
});

const mapStateToProps = ({ taskForm }) => ({
  values: taskForm,
});

export default connect(mapStateToProps, mapDispatchToProps)(PostForm);
