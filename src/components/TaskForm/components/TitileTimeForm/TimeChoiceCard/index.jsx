import React from 'react';
import PropTypes from 'prop-types';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

import SelectionValues from '../../../utils/SelectionValues';
import { Root } from './TimeChoiceCard.style';

const SingleChoiceCard = ({ certainTime, value, onClick }) => (
  <Root>
    <Card
      sx={{ minWidth: 140 }}
      style={{
        backgroundColor: value === certainTime ? '#2B6777' : '#fff',
        color: value === certainTime ? '#FFF' : '#444',
      }}
      onClick={onClick}
    >
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" align="center">
            {SelectionValues.timeSections[certainTime - 1][0]}
          </Typography>
          <Typography gutterBottom variant="span" component="div" align="center">
            {SelectionValues.timeSections[certainTime - 1][1]}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  </Root>
);

SingleChoiceCard.propTypes = {
  certainTime: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default SingleChoiceCard;
