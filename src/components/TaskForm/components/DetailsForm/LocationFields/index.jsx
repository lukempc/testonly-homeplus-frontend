import { React } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextField from '@mui/material/TextField';
import { updateField } from '../../../../../store/reducers/form/form.actions';
import { TextFieldGroup } from '../../../styles/TextFieldGroup.style';

const LocationFields = ({ values, updateFields, first2CateErrors }) => {
  const handleChange = (type) => (e) => {
    updateFields(type, e.target.value);
  };

  return (
    <>
      <h4>Where is the location?</h4>
      <TextFieldGroup>
        <TextField
          required
          id="street"
          label="Street"
          variant="outlined"
          name="title"
          value={values.street}
          onChange={handleChange('street')}
          sx={{ width: 250 }}
          error={first2CateErrors && values.street.length < 5}
          helperText={first2CateErrors && values.street.length < 5 ? 'At least 5 characters' : ' '}
        />
        <TextField
          required
          id="state"
          label="State"
          variant="outlined"
          name="state"
          value={values.state}
          onChange={handleChange('state')}
          sx={{ width: 150 }}
          error={first2CateErrors && values.state === ''}
          helperText={first2CateErrors && values.state === '' ? 'Incorrect or empty' : ' '}
        />
        <TextField
          required
          id="postcode"
          label="Postcode"
          variant="outlined"
          name="postcode"
          value={values.postcode}
          onChange={handleChange('postcode')}
          sx={{ width: 150 }}
          type="number"
          error={first2CateErrors && values.postcode.length !== 4}
          helperText={first2CateErrors && values.postcode.length !== 4 ? 'Incorrect or empty' : ' '}
        />
      </TextFieldGroup>
    </>
  );
};

LocationFields.propTypes = {
  values: PropTypes.object.isRequired,
  updateFields: PropTypes.func,
  first2CateErrors: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  updateFields: (name, value) => dispatch(updateField(name, value)),
});

const mapStateToProps = ({ taskForm }) => ({
  values: taskForm,
});

export default connect(mapStateToProps, mapDispatchToProps)(LocationFields);
